package cn.kopsoft.ems.mapper;

import java.util.List;
import cn.kopsoft.ems.domain.EmsEquipmentType;

/**
 * 设备类型Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
public interface EmsEquipmentTypeMapper 
{
    /**
     * 查询设备类型
     * 
     * @param typeId 设备类型主键
     * @return 设备类型
     */
    public EmsEquipmentType selectEmsEquipmentTypeByTypeId(String typeId);

    /**
     * 查询设备类型列表
     * 
     * @param emsEquipmentType 设备类型
     * @return 设备类型集合
     */
    public List<EmsEquipmentType> selectEmsEquipmentTypeList(EmsEquipmentType emsEquipmentType);

    /**
     * 新增设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    public int insertEmsEquipmentType(EmsEquipmentType emsEquipmentType);

    /**
     * 修改设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    public int updateEmsEquipmentType(EmsEquipmentType emsEquipmentType);

    /**
     * 删除设备类型
     * 
     * @param typeId 设备类型主键
     * @return 结果
     */
    public int deleteEmsEquipmentTypeByTypeId(String typeId);

    /**
     * 批量删除设备类型
     * 
     * @param typeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEmsEquipmentTypeByTypeIds(String[] typeIds);
}
